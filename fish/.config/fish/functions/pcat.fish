function pcat --description "Prints first page of a file and colorises it"

	bat --plain --line-range :39 $argv
end
