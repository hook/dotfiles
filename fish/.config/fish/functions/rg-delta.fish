function rg-delta --description 'RipGrep passed through Delta, to add syntax highlighting and context to the grep output'

	rg --json --context=2 $argv | delta
end
