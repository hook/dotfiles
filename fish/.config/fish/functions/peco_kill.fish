function peco_kill --description "Kill processes by searching it with Peco"

	ps ax -o pid,time,command | peco --query "$LBUFFER" | awk '{print $1}' | xargs kill
end
