function pandoc-document --description 'Pretty PDF document from Markdown'

	pandoc \
		--pdf-engine=xelatex -V papersize:A4 --metadata=geometry:'left=3cm,right=3cm,top=2cm,bottom=2cm' \
		--metadata=colorlinks:true \
		--metadata=author:(getent passwd (whoami) | cut --delimiter ':' --fields 5) \
		--metadata=title=(grep "#" $argv[1] -m1 | cut --delimiter " " --fields 2-) \
		--metadata=date:(date +'%B %Y' --reference $argv[1]) \
		--metadata=toc:true --metadata=toc-depth:3 \
		$argv[1] --output=(basename --suffix=.markdown $argv[1]).pdf $argv[2..]
end
