function node --wraps node --description "Server-side JavaScript runtime"

	__nvm_run "node" $argv
end
