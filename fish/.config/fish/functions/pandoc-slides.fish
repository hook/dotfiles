function pandoc-slides --description 'Pretty PDF slides from Markdown'

	pandoc --to=beamer --pdf-engine=xelatex $argv[1] --output=(basename --suffix=.markdown $argv[1]).pdf $argv[2..]
end
