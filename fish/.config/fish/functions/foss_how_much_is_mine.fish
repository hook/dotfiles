# Defined in - @ line 2
function foss_how_much_is_mine --description "List all source code files do *not* include a string (copyright statement) from the argument. Limited to Java, JavaScript, TypeScript."


	#  Create temporary folder, empty it
	set --global --export foss_how_much_is_mine_tmp_dir /tmp/foss_how_much_is_mine_{$USER}
	if test ! -d {$foss_how_much_is_mine_tmp_dir}
		mkdir {$foss_how_much_is_mine_tmp_dir}
	end
	rm {$foss_how_much_is_mine_tmp_dir}/*

	#  List all Java files (currently lazy scanning)
	set --function foss_how_much_is_mine_extensions java class jar war ear js cjs mjs jsp jsx dpj xrb ts tsx

	for exts in $foss_how_much_is_mine_extensions
		find . -type f -iname "*.$exts" >> {$foss_how_much_is_mine_tmp_dir}/all_files
	end
	sort {$foss_how_much_is_mine_tmp_dir}/all_files | sponge {$foss_how_much_is_mine_tmp_dir}/all_files

	#  List all Java, JS, TS files that include the defined copyright statement (well, string really)
	for file in (cat {$foss_how_much_is_mine_tmp_dir}/all_files)
		grep $argv $file --with-filename >> {$foss_how_much_is_mine_tmp_dir}/my_files
	end
	cat {$foss_how_much_is_mine_tmp_dir}/my_files | cut -f 1 -d : | sort | sponge {$foss_how_much_is_mine_tmp_dir}/my_files

    #  Output diff between the two
	combine {$foss_how_much_is_mine_tmp_dir}/all_files not {$foss_how_much_is_mine_tmp_dir}/my_files > {$foss_how_much_is_mine_tmp_dir}/diff_files
	echo ""
	echo "Binary files missing the defined copyright statements:"
	grep --no-messages {'class$','jar$','war$','ear$'} {$foss_how_much_is_mine_tmp_dir}/diff_files
	echo ""
	echo "Source files missing the defined copyright statements:"
	grep --no-messages {'java$','js$','jsp$','dpj$','xrb$'} {$foss_how_much_is_mine_tmp_dir}/diff_files

	#  Statistics
	echo "---"
	wc -l {$foss_how_much_is_mine_tmp_dir}/all_files
	wc -l {$foss_how_much_is_mine_tmp_dir}/my_files
	wc -l {$foss_how_much_is_mine_tmp_dir}/diff_files
end
