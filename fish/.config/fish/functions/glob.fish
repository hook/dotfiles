function glob --description "Uses `find` to glob through files/folders, since Fish lacks alphabet globbing"

	argparse --ignore-unknown i/case-insensitive -- $argv
	or return

	set --local opt -name
	set --local --query _flag_case-insensitive ; and set opt -iname
	find . -maxdepth 1 $opt $argv
end
