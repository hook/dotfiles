function foss_reuse_for_git_repos --description "Adds REUSE headers with correct years to all files in a Git repo. You need to call if with --license and --copyright flags."

	for file in (git ls-files)
		set --local year (git log --follow --format=%as $file | tail -n1 | cut -c-4)
		reuse addheader --skip-unrecognised --copyright-style spdx-symbol --year $year $argv $file
	end
	reuse download --all
end
