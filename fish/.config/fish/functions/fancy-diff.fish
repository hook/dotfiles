function fancy-diff --description "Character-based colour-coded diff"

	command diff --unified $argv | diff-so-fancy
end
