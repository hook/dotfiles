function nvm --description "Node Version Manager"

	if not type --query bass
		echo 'Bass is not installed please install it running fisher edc/bass'
		return
	end
	set --query NVM_DIR; or set --global --export NVM_DIR ~/.nvm
	set --query nvm_prefix; or set --global --export nvm_prefix $NVM_DIR

	bass source $nvm_prefix/nvm.sh --no-use ';' nvm $argv

	set bstatus $status

	if test $bstatus -gt 0
		return $bstatus
	end

	if test (count $argv) -lt 1
		return 0
	end

	if test $argv[1] = "use"; or test $argv[1] = "install"
		set --global NVM_HAS_RUN 1
	end
end
