function yarn --wraps yarn --description "yarn package manager"

	__nvm_run "yarn" $argv
end
