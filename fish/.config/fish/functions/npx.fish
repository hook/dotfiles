function npx --wraps npx --description "Execute NPM package binaries"

	__nvm_run "npx" $argv
end
