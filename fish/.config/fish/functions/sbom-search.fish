function sbom-search --description "List certain fields from (JSON-formatted) SBOM files"

	argparse 't/sbom-type=!string match --quiet --regex "spdx|cyclonedx" "{$_flag_value}"' 'f/field=!string match --quiet --regex "copyright|declared-license|concluded-license|component-name|component-purl" "{$_flag_value}"' 'h/help' -- $argv
	or return 1

	if set --local --query _flag_help
		echo "Usage: sbom-search OPTION... FILE...

With no FILE, or when FILE is -, read standard input. FILE has to be either an SPDX or CycloneDX file in JSON format.

Both --sbom-type and --field flags are obligatory.

  -t, --sbom-type      what type of SBOM is the can be either: spdx, cyclonedx
  -f, --field          field you are searching for, can be either: component-copyright, component-declared-license, component-concluded-license, component-name, component-purl
  -h, --help           display this help and exit

Examples:
  sbom-search --sbom-type=spdx --field=component-concluded-license package.spdx.json
  sbom-search -tcyclonedx -fcomponent-copyright package.cyclonedx bom.cdx.json
"
		return
	end

	#TODO add more fields to search through (e.g. package copyright/licensing, not just componets)
	#TODO make it so spxd vs cyclonedx is guessed autotmatically (e.g. through extension)
	switch $_flag_sbom_type
		case spdx
			set --global sbom_id_for_components .packages
			switch $_flag_field
				case component-copyright
					set --global sbom_field .copyrightText
				case component-declared-license
					set --global sbom_field .licenseDeclared
				case component-concluded-license
					set --global sbom_field .licenseConcluded
				case component-name
					set --global sbom_field .name
				case component-purl
					set --global sbom_field ".externalRefs | .[] | .referenceLocator"
			end
		case cyclonedx
			set --global sbom_id_for_components .components
			switch $_flag_field
				case component-copyright
					set --global sbom_field .copyright
				case component-declared-license
					set --global sbom_field ".licenses | .[] | .expression"
				case component-concluded-license
					echo "CycloneDX does not distinguish between declared and concluded licenses. Please use `--field=declared-license`."
					# TODO: it does support it since 1.6: <https://cyclonedx.org/docs/1.6/json/#components_items_licenses_oneOf_i0_items_license_acknowledgement>
					return
				case component-name
					set --global sbom_field .name
				case component-purl
					set --global sbom_field .purl
			end
	end

cat $argv | jq "$sbom_id_for_components | .[] | $sbom_field" | sort | uniq

end
