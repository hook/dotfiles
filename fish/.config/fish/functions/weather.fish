function weather --description "Show weather forecast from wttr.io"

	curl http://wttr.in/"$argv"
end
