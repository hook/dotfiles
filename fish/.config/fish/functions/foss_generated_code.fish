# Defined in - @ line 2
function foss_generated_code --description 'List all Java, source code files that are computer generated (and is not "mine" from foss_how_much_is_mine)'

	grep "@generated" (cat {$foss_how_much_is_mine_tmp_dir}/diff_files) --files-with-match
end
