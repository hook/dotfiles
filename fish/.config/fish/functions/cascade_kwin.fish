function cascade_kwin --description "Reorganise windows in KWin into a cascade"

	qdbus org.kde.KWin /KWin cascadeDesktop
end
