function fish_greeting --description "What's up, fish?"

	set_color $fish_color_autosuggestion
	uname --nodename --machine --kernel-name --kernel-release

	command --quiet --search uptime >/dev/null
	and command uptime

	set_color normal
end
