function convert-scanned-pngs-to-pdf --description "Create a reasonably-sized PDF from a bunch of scanned PNGs"

	for img_png in (ls $argv)
		magick $img_png -resample 70% (basename {$img_png} .png).jpg
		set --append jpgs (basename {$img_png} .png).jpg
	end
	magick $jpgs converted.pdf
end
