function foss_scancode_filename --description "List all info about a filename from ScanCode JSON (first search name, then ScanCode file)"

	rg --before-context=3 --after-context=31 "\"name\": \"$argv[1]" $argv[2] | bat --language json
end
