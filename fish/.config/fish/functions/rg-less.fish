function rg-less --description "RipGrep pagenated through Less, but with keeping the grep highlights"

	rg --pretty $argv | less --raw-control-chars
end
