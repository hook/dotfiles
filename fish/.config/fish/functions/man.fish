function man --wraps man --description "Format and display manual pages"

	set --query man_blink; and set --local blink (set_color $man_blink); or set --local blink (set_color --bold red)
	set --query man_bold; and set --local bold (set_color $man_bold); or set --local bold (set_color --bold cyan)
	set --query man_standout; and set --local standout (set_color $man_standout); or set --local standout (set_color blue)
	set --query man_underline; and set --local underline (set_color $man_underline); or set --local underline (set_color --underline yellow)

	set --local end (printf "\e[0m")

	set --local --export LESS_TERMCAP_mb $blink
	set --local --export LESS_TERMCAP_md $bold
	set --local --export LESS_TERMCAP_me $end
	set --local --export LESS_TERMCAP_so $standout
	set --local --export LESS_TERMCAP_se $end
	set --local --export LESS_TERMCAP_us $underline
	set --local --export LESS_TERMCAP_ue $end
	set --local --export LESS '-R -s'

	set --local --export GROFF_NO_SGR yes # fedora

	set --local --export MANPATH (string join : $MANPATH)
	if test -z "$MANPATH"
		type --query manpath
		and set MANPATH (command manpath)
	end

	# Check data dir for Fish 2.x compatibility
	set --local fish_data_dir
	if set --query __fish_data_dir
		set fish_data_dir $__fish_data_dir
	else
		set fish_data_dir $__fish_datadir
	end

	set --local fish_manpath (dirname $fish_data_dir)/fish/man
	if test -d "$fish_manpath" -a -n "$MANPATH"
		set MANPATH "$fish_manpath":$MANPATH
		command man $argv
		return
	end
	command man $argv
end
