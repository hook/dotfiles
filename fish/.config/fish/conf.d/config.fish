if status is-interactive
    # Commands to run in interactive sessions can go here


	### Makes sure how much directories are shortened (0 = none)

	set --global fish_prompt_pwd_dir_length 5


	### Hydro theme settings

	set --global hydro_multiline true
	# Sets a different color for the path for root and normal user
	if fish_is_root_user
		set --global hydro_color_pwd $fish_color_cwd_root
	else
		set --global hydro_color_pwd $fish_color_cwd
	end
	set --global hydro_color_git $fish_color_host_remote
	set --global hydro_color_prompt $fish_color_user
	set --global hydro_color_duration $fish_color_autosuggestion

	### BobTheFish theme settings

# 	set --global theme_nerd_fonts yes
# 	set --global theme_color_scheme dark
# 	set --global theme_newline_cursor yes


	### Key bindings

	bind \cr 'peco_select_history (commandline -b)' # Ctrl+r to use peco for searching history

end
