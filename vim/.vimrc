filetype plugin on
filetype indent on

set encoding=utf-8

"Sets tabulation to 4 spaces (and that 4 spaces equal a tab).
set tabstop=4
set shiftwidth=4
set softtabstop=4

"Disables Vi legacy, thus enabling some more fancy features.
set nocompatible

"Enables the status bar.
"(so I can see the insert/normal/visual mode and where I am in the file)
set laststatus=2

"Enables Syntax highlighting
syntax enable
"Enables 256 colours (or not)
"set t_Co=256
"Applies the colourscheme
colorscheme default
"Sets the background colour to 'dark' or 'light'
set background=dark

"Enables the menu for auto-completion
set wildmenu

"Stops whining when I try to hide an unsaved buffer.
set hidden

"Shows previous command.
set showcmd

"Because pretty much everyone and their dog uses a fast TTY nowadays.
set ttyfast

"Makes copy-pasting with mouse work as expected (solves the indenting bug).
set paste

"Minimum distance from cursor to the edge of the screen.
set scrolloff=3

"Custom tabulator and end-of-line characters.
set listchars=tab:▸\ ,eol:↵
"Shows/hides these special characters → <leader> l
nmap <silent> <leader>l :set list!<CR>

"Clears search selection → <leader> <space>
nnoremap <silent> <leader><space> :noh<cr>

"Shows/hides line numbers → <leader> n
nnoremap <silent> <leader>n :set number!<cr>

"Shows/hides relative line numbers → <leader> N
nnoremap <silent> <leader>N :set relativenumber!<cr>

"Enables undo/backup files.
"(depends on Vim 7.3)
set undofile

"Hack, to disable Vim-specific RegEx and enables Python/Perl-like RegEx.
nnoremap / /\v
vnoremap / /\v

"Makes search more logical (to someone like me).
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch

"With <tab> you can jump between beginning and ending brackets.
nnoremap <tab> %
vnoremap <tab> %

"Enables en_GB as default spell checking and sets the a shortcut for spell checking → <leader> s
set spelllang=en_gb
nmap <silent> <leader>s :set spell!<CR>

"Shows the line and column location of the cursor.
set ruler

"Show advanced tree-structured undo panel → <F5>
"(depends on Gundo)
nnoremap <F5> :GundoToggle<CR>

"TODO — have window movement, resizing, focus and splitting the same as in Tmux.

"When Ctrl+SpaceBar are pressed, it flashes the cursor line and column. Hack found on:  http://briancarper.net/blog/590/cursorcolumn--cursorline-slowdown
function! CursorPing()
	set cursorline cursorcolumn
	redraw
	sleep 50m
	set nocursorline nocursorcolumn
endfunction

nmap <C-Space> :call CursorPing()<CR>
