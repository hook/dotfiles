// SPDX-License-Identifier: CC0-1.0
// SPDX-FileCopyrightText: © 2021 Matija Šuklje <matija@suklje.name>

// Enables custom chrome
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// Enables XDG Desktop Portal integration (e.g. to use KDE Plasma file picker)
// https://wiki.archlinux.org/title/Firefox#XDG_Desktop_Portal_integration
user_pref("widget.use-xdg-desktop-portal.file-picker", 1);
user_pref("widget.use-xdg-desktop-portal.mime-handler", 1);
user_pref("widget.use-xdg-desktop-portal.settings", 1);
user_pref("widget.use-xdg-desktop-portal.location", 1);
user_pref("widget.use-xdg-desktop-portal.open-uri", 1);

// Enables further KDE integration (to disable duplicated entry in Media Player)
// https://wiki.archlinux.org/title/Firefox#KDE_integration
user_pref("media.hardwaremediakeys.enabled", false);