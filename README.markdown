# Installation

Obviously, you first need to have [GNU Stow][stow] installed.

Then it is a simple matter of pulling the dotfiles repository:

```sh
git clone https://gitlab.com/hook/dotfiles.git ~/dotfiles
cd ~/dotfiles
git submodule init
git submodule update
stow fish vim tmux   # plus/minus whatever else you like
```

With which you:

1. check out the latest version of these dotfiles’ Git repository into your `~/dotfiles` directory (feel free to change it to `~/.dotfiles`, if you prefer),
1. change into the `~/dotfiles` directory, so Stow is in the right directory relative to the configuration files and folders we want to populate,
1. initialise and update the submodules (e.g. `ranger_devicons`)
1. using `stow` to create the the links to the right configuration files (in the right directories) – in the above example that would be the contents of the `~/dotfiles/fish`, `~/dotfiles/vim` and `~/dotfiles/tmux`. Substitute that for the folders that you are actually interested in.

For dependencies for any individual dotfiles see the [Dependencies](#dependencies) section below.

[stow]: https://www.gnu.org/software/stow/

## Update

You can later update the dotfiles (e.g. for Vim) to sync them with this repository by running:

```sh
cd ~/dotfiles
git pull
git submodule update
stow --restow vim
```

## Remove

If you do not want to use a specific dotfile directory any more (e.g. Tmux), do:

```sh
cd ~/dotfiles
stow --delete tmux
```


# Usage

## Convert Scanned PNGs to PDF

Takes a list of `.png` files and reduces their size, converts them to JPEG and stores them in a PDF. Useful, when you scan long documents (and cannot be arsed to change the scanner settings).

([Glob](#glob) is very useful here.)

## Git

I decided to store my Git (and [Delta][delta]) config in Stow too. Any authentication or other user/computer-specific configurations should go into `~/.config/git/user`, which is explicitly ignored by this repository.

## Glob

Since [globbing for letters (alphabet characters) is not implemented yet in Fish Shell][fish_bug_glob], with _ample_ help of Fabian Boehm (fabo), I am using the `glob` function to mitigate this shortcoming.

`glob` is essentially just a wrapper for `find`, so it will output any file or folder it finds with the argument you give it. It uses an optional flag `--case-insensitive` / `-i`, which, as the name implies, makes the search case-insensitive.

For example running the following in `dotfiles/fish/.config/fish/functions`:

```sh
glob '[a-c,e]*'
```

will result in:
```sh
./bass.fish
./cascade_kwin.fish
./cpb.fish
./eza.fish
./convert-scanned-pngs-to-pdf.fish
```

Not very useful by itself, but if you wrap it in brackets, it can replace the aforementioned current shortcoming of Fish.

So, where you would expect `git add [a-c,e]*` to work, instead, you pass the glob through `glob` first, like this:

```sh
git add (glob '[a-c,e]*')
```

[fish_bug_glob]: https://github.com/fish-shell/fish-shell/issues/3681

## i3 

The biggest change is that I use the `Meta` (`Win`) key as the modifier for switching between virtual desktops/tags and all similar window-related tasks.

Apart from that there are typical keybindings for screen locking, volume and screen brightness.

Make sure to `stow i3 dunst` to also have 

## Tmux

Most changes make the keybindings closer to GNU Screen (e.g. `Ctrl+A` instead of the default `Ctrl+B`) and Vim.

Most other changes are just small visual niceties, plus the small mem-cpu-load monitor in the corner.

## Pcat

`pcat $filename` will print the first 39 lines of the file and apply syntax highlighting.

## Aerc

[Aerc][aerc] is a modern, terminal-based e-mail client that is easier to use than Mutt, and more powerful than Pine.

The dotfiles clearly do not include `~/.config/aerc/accounts.conf`, so you need to create those yourself – either by hand or generate them when running `aerc` for the first time.

[aerc]: https://aerc-mail.org/

## Ranger

[Ranger][ranger] is a pretty neat CLI file manager, which I occasionaly use. So far the only change is that I use the [Devicons plugin][devicons] to show Nerd font icons in it.

[ranger]: https://ranger.github.io/
[devicons]: https://github.com/alexanderjeurissen/ranger_devicons

## Firefox template

The `firefox_template` folder is a bit special, as you cannot simply `stow` it, because Firefox can have several profiles and therefore they have different names, to not mess with each other.

So to use it, you need to store the content of this folder to your (template) profile, e.g. `~/.mozilla/firefox/????????.default` for default or `~/.mozilla/firefox/????????.template-profile` for template profile.

The `user.js` and `chrome/userChrome.css` tweaks to Firefox are to make it integrate better with KDE Plasma as well as with [Sidebery][] (or any other vertical tabs like [Tree Style Tabs][tst]).

[sidebery]: https://addons.mozilla.org/en-US/firefox/addon/sidebery/
[tst]: https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab

## FOSS/copyright compliance tools

For my FOSS/copyright compliance work, I created some short scripts to assist me do some common chores that include regular expressions, querying JSON and suchlike, which I find tedious to remember. These all start with `foss_*`. I am listing them in descending order of wider audience usefuleness.

### REUSE wrapper for existing Git repos

Following the specification on <https://reuse.software> is a great way annotate your own code with appropriate license and copyright notices. It is also super simple to use the [`reuse` tool][reuse] itself when you start a new repository. But if you are trying to apply it to a pre-existing repository with files of varying age, especially if you want the [copyright statements to be exact][copyright_statements_blog].

[copyright_statements_blog]: https://matija.suklje.name/how-and-why-to-properly-write-copyright-statements-in-your-code 

This is what `foss_reuse_for_git_repos` helps with – it finds the date of the first commit of each file and uses that year in the copyright statement in the REUSE header. For massive repos with a lot of history, this can take some time.

For example, if you want to license an existing repositry under the _MIT_ license and your name is “Da Hacker” you can do the following:

```sh
cd $git_repo
foss_reuse_for_git_repos --copyright "Da Hacker <me@hacker.example>" --license MIT
```

### SBOM (SPDX, CycloneDX) queries

There are two major Software Bill of Material (SBOM) standards in use – [SPDX][] and [CycloneDX][]. Previously I relied on `grep` to search through SPDX tag:value format, but using `jq` to parse their JSON formats produces more reliable results (at least with my limited skills). As I still find `jq` a bit convoluted to use and its documentation quite arcane, I created the `sbom-search` function to handle that.

It can process both SPDX JSON with the `--sbom-type=spdx` (or `-tspdx`) flag and CycloneDX JSON with the `--sbom-type=cyclonedx` (or `-tcyclonedx`) flag.

Currently you can search for the following fields by use of `--field=` (or `-f`) flag:

- `component-copyright` – lists copyright notices of all of the components in the package
- `component-declared-license` – list all declared licenses of all of the components in the package
- `component-concluded-license` – lists all concluded licenses of all of the components in the package
- `component-name` – lists all names of the components in the package; and more reliably …
- `component-purl` – lists all [Package URLs][purl] of all the components in the package

It even has a `--help` flag `:)`.


Examples:

```sh
sbom-search --sbom-type=spdx --field=component-concluded-license package.spdx.json
```

```sh
sbom-search -tcyclonedx -fcomponent-copyright package.cyclonedx bom.cdx.json
```

I will likely add more fields to that later.

[CycloneDX]: https://cyclonedx.org
[SPDX]: https://spdx.dev
[purl]: https://github.com/package-url/purl-spec

### Find file in ScanCode results

In a similar vain, `foss_scancode_filename {$searched_filename} {$scancode_file}` lists whole JSON blocks from a ScanCode results file that match the `$searched_filename` and applies syntax highlighting to it.

This can be useful when you are trying to figure out where (if at all) a certain file comes in and what information is attached to it.

### Copyright statement and generated code finder

These tackle a very specific use case, are very stupid and quite hackish:

- `foss_how_much_is_mine` – lists all Java, JavaScript and TypeScript source (and some binary) code files that do *not* include a string (intent: copyright statement) passed through the argument
- `foss_generated_code` – lists all Java, JavaScript and TypeScript source (and some binary) code files that do not inlude the above string and are also marked as `@generated`

If you want to run `foss_generated_code` you need to first run `foss_how_much_is_mine`. 

Example:

First run:

```sh
cd {$my_source_code}
foss_how_much_is_mine "BigAss Company, Inc"
```

This will list all files that do _not_ include the string "BigAss Company, Inc" (and therefore we assume are not © BigAss Company) as well as provide a count of all files analysed, those that include said string, and those that do not.

If you then run in the same directory also:

```sh
foss_generated_code
```

It will list all the files that do _not_ include the above string, but that do include the string `@generated`.


# Dependencies

## i3

In addition to [i3][] itself, the following things are needed for some of the functionalities to work as intended:

- [i3-volume][] and [dunst][] for volume control and its OSD (see i3-volume docs for OSD alternatives))
- [xbacklight-ctl][] and [dunst][] for brightness control and its OSD
- compton/[picon][] compositor to enable transparency
- [terminology][] as the terminal emulator (just change it for whatever you prefer)
- [feh][] image viewer to set background images
- [rofi][] as a window switcher and application launcher
- [i3lock-fancy][] to lock the screen in a pretty manner

[i3]: https://i3wm.org/
[i3-volume]: https://github.com/hastinbe/i3-volume
[dunst]: https://dunst-project.org/
[i3lock-fancy]:https://github.com/meskarune/i3lock-fancy
[feh]: https://feh.finalrewind.org/
[picon]: https://github.com/yshui/picom
[rofi]: https://github.com/davatorium/rofi
[terminology]: https://www.enlightenment.org/about-terminology.md
[xbacklight-ctl]: https://github.com/EnmanuelVT/xbacklight-ctl

## Fish shell

In addition to [Fish][fish] itself, the following functions have the following run-time dependencies:

- the `Ctrl+R` key binding for easier searching through history depends on [peco][]
- `cascade_kwin` depends on [KDE’s window manager KWin][kde]
- ~`fancy-diff` is a wrapper for [diff-so-fancy][dsf]~ – I recently moved to [delta][] instead
- `foss_how_much_is_mine` depends on [moreutils][]
- `foss_reuse_for_git_repos` is a wrapper for the [reuse][] helper tool and Git
- ~ls` has [lsd][] as an optional dependency, if you have `lsd` installed it will used that, if not, just standard `ls`~ – I recently moved to [eza][] instead
- `pandoc-document` and `pandoc-slides` depend on [Pandoc][pandoc]
- `pcat` depends on [bat][bat]
- `rg-delta` depends on [RipGrep][ripgrep] and [delta][delta]
- `sbom-search` depends on [jq][] to query JSON
- ~`tree` depends on [tree][] (and optionally on [lsd][])~ – I recently moved to [eza][] instead

Not exactly a dependency, but I incorporated the [Hydro][] Fish prompt and modified it slighlty. And to have the `ssh-agent` work nicely in Fish, I am using [fish-ssh-agent][].

My set-up also assumes use of [Nerd fonts][nerd].

[bat]: https://github.com/sharkdp/bat
[delta]: https://github.com/dandavison/delta
[dsf]: https://github.com/so-fancy/diff-so-fancy
[eza]: https://eza.rocks/
[fish]: https://fishshell.com/
[fish-ssh-agent]: https://github.com/danhper/fish-ssh-agent
[gtimelog]: https://mg.pov.lt/gtimelog/
[hydro]: https://github.com/jorgebucaran/hydro/
[jq]: https://stedolan.github.io/jq/
[kde]: https://kde.org
[lsd]: https://github.com/Peltoche/lsd
[moreutils]: https://joeyh.name/code/moreutils/
[nerd]: https://www.nerdfonts.com/
[pandoc]: http://pandoc.org/
[peco]: https://github.com/peco/peco
[reuse]: https://reuse.software/dev/#tool
[ripgrep]: https://github.com/BurntSushi/ripgrep
[tree]: http://mama.indstate.edu/users/ice/tree/
[vim]: http://www.vim.org/


## Tmux

This [tmux][] config file is written in a way to show RAM and CPU usage as well as the system’s load.

For this to work, you have to install [tmux-mem-cpu-load][tmux-mem].

[tmux]: https://tmux.github.io/
[tmux-mem]: https://github.com/thewtex/tmux-mem-cpu-load


# Troubleshooting

## Stow complains

If e.g. during your first `stow fish` Stow complains like this:

```sh
WARNING! stowing fish would cause conflicts:
  * existing target is neither a link nor a directory: .config/fish/config.fish
All operations aborted.
```

Then simply remove the conflicting file (in this case `~/.config/fish/config.fish`) and re-run the command that failed.
